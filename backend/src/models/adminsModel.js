const admins = (sequelize, DataTypes) => {
    const Admins = sequelize.define('admins', {
        adminId: {
            allowNull: false, // 允不允許Null
            primaryKey: true, // 設立主鍵
            type: DataTypes.INTEGER, //  資料型態
            autoIncrement: true, // 流水號
        },
        name: {
            allowNull: false,
            type: DataTypes.STRING(30),
        },
        account: {
            allowNull: false,
            type: DataTypes.STRING(30),
        },
        password: {
            allowNull: false,
            type: DataTypes.STRING(200),
        },
        adminStatus: {
            allowNull: false,
            type: DataTypes.BOOLEAN
        },
        creatAt: {
            allowNull: false,
            type: DataTypes.DATE,
        }

    }, {
        timestamps: false, // 是否自動建立時間戳記
    });

    return Admins;
}

export { admins }