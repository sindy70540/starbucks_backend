import { Sequelize, DataTypes } from 'sequelize';
import databaseService from '../services/databaseService';

import { admins } from '../models/adminsModel';
// import { admins } from '../models/adminsModel';

class Database {
    // 透過constructor(建構子)的方式，當該Class變實作時，會自動執行
    constructor() {
        const db = {};
        // 開始連線
        const sequelize = databaseService.connect();

        // 建立ORM映射環境
        db.Sequelize = Sequelize;
        db.sequelize = sequelize;

        db.Models = {};
        db.Models.admins = admins(sequelize, DataTypes);

        return db;
    }
}

export default new Database();