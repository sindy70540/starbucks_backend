import { Router } from 'express';
// const { Router } = require('express');

const router = Router();

/* GET index page. */
router.get('/', (req, res) => {
  res.json({
    title: 'EXPRESS',
  });
});

export default router;